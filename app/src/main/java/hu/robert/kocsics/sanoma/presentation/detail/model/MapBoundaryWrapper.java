package hu.robert.kocsics.sanoma.presentation.detail.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.List;

import hu.robert.kocsics.sanoma.data.model.Poi;

public class MapBoundaryWrapper {
    private LatLngBounds mapBoundary;
    private List<Poi> markers;

    public MapBoundaryWrapper(LatLngBounds mapBoundary, List<Poi> markers) {
        this.mapBoundary = mapBoundary;
        this.markers = new ArrayList<>(markers);
    }

    public boolean isVisibleInBoundary(Poi poi) {
        return mapBoundary.contains(new LatLng(poi.lat, poi.lon));
    }

    public List<Poi> getMarkers() {
        return markers;
    }
}
