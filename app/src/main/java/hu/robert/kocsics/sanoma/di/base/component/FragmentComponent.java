package hu.robert.kocsics.sanoma.di.base.component;

import dagger.Subcomponent;
import hu.robert.kocsics.sanoma.di.base.PerFragment;
import hu.robert.kocsics.sanoma.di.base.module.FragmentModule;

/**
 * This component inject dependencies to all Fragments across the application
 */
@PerFragment
@Subcomponent(modules = FragmentModule.class)
public interface FragmentComponent {
}
