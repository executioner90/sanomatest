package hu.robert.kocsics.sanoma.presentation.main;

import java.util.List;

import hu.robert.kocsics.sanoma.data.model.Poi;
import hu.robert.kocsics.sanoma.presentation.base.MvpView;
import io.reactivex.Observable;

public interface MainView extends MvpView {

    void showPoiList(List<Poi> poiList);

    void showProgress(boolean show);

    void showError(Throwable error);

    void showOnMap(Poi poi);

    void showAllOnMap();

    Observable<Poi> getPoiClick();
}
