package hu.robert.kocsics.sanoma.presentation.detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import hu.robert.kocsics.sanoma.R;
import hu.robert.kocsics.sanoma.data.model.Poi;
import hu.robert.kocsics.sanoma.di.component.ActivityComponent;
import hu.robert.kocsics.sanoma.presentation.base.BaseActivity;
import hu.robert.kocsics.sanoma.presentation.detail.model.MapBoundaryWrapper;
import hu.robert.kocsics.sanoma.presentation.detail.model.PoiDetailViewModel;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public class PoiDetailActivity extends BaseActivity implements PoiDetailView, OnMapReadyCallback {
    public static final String EXTRA_POI = "EXTRA_POI";

    // the boundary will multiplied by this factor for showing markers
    static final float MAP_DIAMETER_FACTOR = 2f;

    private Subject<Poi> poiClickSubject;
    private Subject<MapBoundaryWrapper> boundarySubject;
    private Subject<Boolean> mapLoadedSubject;

    private Map<String, Marker> markerMap;

    private boolean isCameraIdle;
    private boolean isCameraAnimateEnabled = true;

    @Inject
    PoiDetailPresenter presenter;

    // UI
    GoogleMap googleMap;

    public static Intent createIntent(Context context, @Nullable Poi poi) {
        Intent intent = new Intent(context, PoiDetailActivity.class);
        if (poi != null) {
            intent.putExtra(EXTRA_POI, poi);
        }
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        poiClickSubject = PublishSubject.create();
        boundarySubject = PublishSubject.create();
        mapLoadedSubject = PublishSubject.create();
        markerMap = new HashMap<>();

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        isCameraAnimateEnabled = savedInstanceState == null;
        if (getIntent() != null && getIntent().getSerializableExtra(EXTRA_POI) != null) {
            presenter.showPoiFromIntent((Poi) getIntent().getSerializableExtra(EXTRA_POI));
        }
        presenter.onViewCreated();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // base activity

    @Override
    protected int getLayout() {
        return R.layout.activity_poi_detail;
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    protected void attachView() {
        presenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        presenter.detachView();
    }

    // OnMapReadyCallback

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        setMapBoundary();

        googleMap.setOnMarkerClickListener(marker -> {
            poiClickSubject.onNext((Poi) marker.getTag());
            return false;
        });
        googleMap.setOnCameraMoveListener(() -> isCameraIdle = false);
        googleMap.setOnCameraIdleListener(() -> {
            if (!isCameraIdle) {
                setMapBoundary();
            }
            isCameraIdle = true;
        });
        googleMap.setOnMapLoadedCallback(() -> mapLoadedSubject.onNext(true));
    }

    // PoiDetailView

    @Override
    public void setToolbarTitle(String title) {
        setTitle(title);
    }

    @Override
    public void displayMarkers(PoiDetailViewModel poiDetailViewModel) {
        CameraUpdate update = null;

        // remove not visible markers
        if (!poiDetailViewModel.getPoisToRemove().isEmpty()) {
            for (Poi poi : poiDetailViewModel.getPoisToRemove()) {
                Marker marker = markerMap.get(poi.name);
                if (marker != null) {
                    marker.remove();
                    markerMap.remove(poi.name);
                }
            }
        }

        // add new markers
        if (poiDetailViewModel.getPoisToAdd().size() == 1) {
            Poi poi = poiDetailViewModel.getPoisToAdd().get(0);
            LatLng mapPoi = new LatLng(poi.lat, poi.lon);
            googleMap.addMarker(new MarkerOptions().position(mapPoi).title(poi.name));
            if (isCameraAnimateEnabled) {
                update = CameraUpdateFactory.newLatLngZoom(mapPoi, 14);
            }
        } else {
            LatLngBounds.Builder builder = null;
            if (isCameraAnimateEnabled) {
                builder = new LatLngBounds.Builder();
            }
            for (Poi poi : poiDetailViewModel.getPoisToAdd()) {
                LatLng mapPoi = new LatLng(poi.lat, poi.lon);
                Marker marker = googleMap.addMarker(new MarkerOptions().position(mapPoi).title(poi.name));
                marker.setTag(poi);
                if (builder != null) {
                    builder.include(marker.getPosition());
                }
                markerMap.put(poi.name, marker);
            }
            if (builder != null) {
                LatLngBounds bounds = builder.build();
                update = CameraUpdateFactory.newLatLngBounds(bounds, 30);
            }
        }
        if (update != null) {
            googleMap.moveCamera(update);
        }
        isCameraAnimateEnabled = false;
    }

    @Override
    public Observable<Boolean> mapLoaded() {
        return mapLoadedSubject;
    }

    @Override
    public Observable<Poi> getPoiClick() {
        return poiClickSubject;
    }

    @Override
    public Observable<MapBoundaryWrapper> onBoundaryChange() {
        return boundarySubject;
    }

    // private methods

    private void setMapBoundary() {
        LatLngBounds latLngBounds = googleMap.getProjection().getVisibleRegion().latLngBounds;
        // can't work with markers on non ui thread, transform to poi list
        List<Poi> pois = new ArrayList<>();
        for (Marker marker : markerMap.values()) {
            pois.add((Poi) marker.getTag());
        }
        boundarySubject.onNext(new MapBoundaryWrapper(createBiggerBound(latLngBounds), pois));
    }

    // bound translators
    private LatLng calculateBoundsPoint(LatLng center, LatLng point) {
        return new LatLng((point.latitude - center.latitude) * MAP_DIAMETER_FACTOR + center.latitude, (point.longitude - center.longitude)
                * MAP_DIAMETER_FACTOR + center.longitude);
    }

    private LatLngBounds createBiggerBound(LatLngBounds bounds) {
        return new LatLngBounds(calculateBoundsPoint(bounds.getCenter(), bounds.southwest),
                calculateBoundsPoint(bounds.getCenter(), bounds.northeast));
    }


}
