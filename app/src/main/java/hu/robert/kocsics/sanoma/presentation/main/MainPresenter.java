package hu.robert.kocsics.sanoma.presentation.main;

import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import hu.robert.kocsics.sanoma.domain.PoiRepository;
import hu.robert.kocsics.sanoma.data.model.Poi;
import hu.robert.kocsics.sanoma.presentation.base.BasePresenter;
import hu.robert.kocsics.sanoma.di.base.ConfigPersistent;
import hu.robert.kocsics.sanoma.util.rx.scheduler.SchedulerUtils;
import io.reactivex.functions.Consumer;

@ConfigPersistent
public class MainPresenter extends BasePresenter<MainView> {

    private final PoiRepository poiRepository;

    @Inject
    MainPresenter(PoiRepository poiRepository) {
        this.poiRepository = poiRepository;
    }

    // public methods

    public void onViewCreated() {
        checkViewAttached();
        addDisposable(getView().getPoiClick()
                .subscribe(
                        poi -> {
                            checkViewAttached();
                            getView().showOnMap(poi);
                        },
                        throwable -> Log.e("MainActivity", "Poi click failed", throwable)));
        getPoiList(false);
    }

    public void onReloadClicked() {
        getPoiList(true);
    }

    public void showAllTapped() {
        getList(pois -> {
            checkViewAttached();
            getView().showAllOnMap();
        }, false);
    }

    // private

    private void getPoiList(boolean forced) {
        checkViewAttached();
        getView().showProgress(true);
        getList(pois -> {
            checkViewAttached();
            getView().showProgress(false);
            getView().showPoiList(pois);
        }, forced);
    }

    private void getList(Consumer<List<Poi>> onNext, boolean forced) {
        addDisposable(poiRepository
                .getPoiList(forced)
                .compose(SchedulerUtils.ioToMain())
                .subscribe(
                        onNext,
                        throwable -> {
                            checkViewAttached();
                            getView().showProgress(false);
                            getView().showError(throwable);
                        }));
    }
}
