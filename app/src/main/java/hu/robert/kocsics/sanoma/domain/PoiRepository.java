package hu.robert.kocsics.sanoma.domain;

import android.annotation.SuppressLint;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import hu.robert.kocsics.sanoma.data.model.Poi;
import hu.robert.kocsics.sanoma.data.service.PoiService;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.subjects.BehaviorSubject;

@Singleton
public class PoiRepository {

    private PoiService poiService;
    private Scheduler scheduler;

    private BehaviorSubject<List<Poi>> listBehaviorSubject;

    @Inject
    PoiRepository(PoiService poiService, Scheduler scheduler) {
        this.poiService = poiService;
        this.scheduler = scheduler;
    }

    @SuppressLint("CheckResult")
    public Observable<List<Poi>> getPoiList(boolean forced) {
        if (listBehaviorSubject == null || forced) {
            listBehaviorSubject = BehaviorSubject.create();
            poiService
                    .getPoiList()
                    .subscribeOn(scheduler)
                    .subscribe(pois -> listBehaviorSubject.onNext(pois.pois),
                            listBehaviorSubject::onError);
        }
        return listBehaviorSubject;
    }
}
