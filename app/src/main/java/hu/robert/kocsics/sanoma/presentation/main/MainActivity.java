package hu.robert.kocsics.sanoma.presentation.main;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import hu.robert.kocsics.sanoma.R;
import hu.robert.kocsics.sanoma.data.model.Poi;
import hu.robert.kocsics.sanoma.presentation.base.BaseActivity;
import hu.robert.kocsics.sanoma.presentation.common.ErrorView;
import hu.robert.kocsics.sanoma.presentation.detail.PoiDetailActivity;
import hu.robert.kocsics.sanoma.di.component.ActivityComponent;
import io.reactivex.Observable;

public class MainActivity extends BaseActivity implements MainView, ErrorView.ErrorListener {

    // dependencies
    @Inject
    PoiAdapter poiAdapter;
    @Inject
    MainPresenter mainPresenter;

    // UI
    @BindView(R.id.recycler_poi)
    RecyclerView poiRecycler;

    @BindView(R.id.view_error)
    ErrorView errorView;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    // lifecycle & base methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);

        poiRecycler.setLayoutManager(new LinearLayoutManager(this));
        poiRecycler.setAdapter(poiAdapter);

        errorView.setErrorListener(this);

        mainPresenter.onViewCreated();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_map) {
            mainPresenter.showAllTapped();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // base activity

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void inject(ActivityComponent activityComponent) {
        activityComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mainPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mainPresenter.detachView();
    }

    // MainView

    @Override
    public void showPoiList(List<Poi> poiList) {
        poiAdapter.setPoiList(poiList);
        poiRecycler.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress(boolean show) {
        if (show) {
            if (poiRecycler.getVisibility() == View.VISIBLE
                    && poiAdapter.getItemCount() > 0) {
            } else {
                progressBar.setVisibility(View.VISIBLE);
                poiRecycler.setVisibility(View.GONE);
            }
            errorView.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showError(Throwable error) {
        poiRecycler.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showOnMap(Poi poi) {
        startActivity(PoiDetailActivity.createIntent(this, poi));
    }

    @Override
    public void showAllOnMap() {
        startActivity(PoiDetailActivity.createIntent(this, null));
    }

    @Override
    public Observable<Poi> getPoiClick() {
        return poiAdapter.getPoiClick();
    }

    // ErrorListener

    @Override
    public void onReloadData() {
        mainPresenter.onReloadClicked();
    }
}
