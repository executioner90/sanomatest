package hu.robert.kocsics.sanoma.presentation.detail;

import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import hu.robert.kocsics.sanoma.data.model.Poi;
import hu.robert.kocsics.sanoma.di.base.ConfigPersistent;
import hu.robert.kocsics.sanoma.domain.PoiRepository;
import hu.robert.kocsics.sanoma.presentation.base.BasePresenter;
import hu.robert.kocsics.sanoma.presentation.detail.model.MapBoundaryWrapper;
import hu.robert.kocsics.sanoma.presentation.detail.model.PoiDetailViewModel;
import hu.robert.kocsics.sanoma.util.rx.scheduler.SchedulerUtils;
import io.reactivex.Observable;

@ConfigPersistent
public class PoiDetailPresenter extends BasePresenter<PoiDetailView> {
    private static final int MAX_VISIBLE_MARKERS = 100;

    // dependency
    private final PoiRepository poiRepository;

    // state
    private Poi poi;

    @Inject
    PoiDetailPresenter(PoiRepository poiRepository) {
        this.poiRepository = poiRepository;
    }

    // public methods

    public void showPoiFromIntent(@Nullable Poi poi) {
        this.poi = poi;
    }

    public void onViewCreated() {
        checkViewAttached();

        addDisposable(getView().getPoiClick()
                .subscribe(
                        poi -> {
                            // TODO show something on poi click
                        },
                        throwable -> Log.e("MainActivity", "Poi click failed", throwable)));

        if (poi == null) {
            getView().setToolbarTitle("Pois on the Map");
            addDisposable(Observable.combineLatest(getView().mapLoaded(), getView().onBoundaryChange(),
                    poiRepository.getPoiList(false), (aBoolean, mapBoundaryWrapper, pois)
                            -> calculatePois(pois, mapBoundaryWrapper)).compose(SchedulerUtils.ioToMain())
                    .subscribe(poiDetailViewModel -> {
                        checkViewAttached();
                        getView().displayMarkers(poiDetailViewModel);
                    }));
        } else {
            getView().setToolbarTitle(poi.name);
            addDisposable(Observable.combineLatest(getView().mapLoaded(), Observable.just(poi),
                    (aBoolean, poi) -> {
                        List<Poi> pois = new ArrayList<>();
                        pois.add(poi);
                        return new PoiDetailViewModel(pois, Collections.emptyList());
                    }
            ).compose(SchedulerUtils.ioToMain()).subscribe(poiDetailViewModel -> {
                checkViewAttached();
                getView().displayMarkers(poiDetailViewModel);
            }));
        }
    }

    // private

    private PoiDetailViewModel calculatePois(List<Poi> pois, MapBoundaryWrapper mapBoundaryWrapper) {
        List<Poi> visiblePois = new ArrayList<>();
        List<Poi> removePois = new ArrayList<>();

        // remove not visible pois
        for (Poi poi : mapBoundaryWrapper.getMarkers()) {
            if (!mapBoundaryWrapper.isVisibleInBoundary(poi)) {
                removePois.add(poi);
            }
        }

        // if no new poi, return
        if (pois.isEmpty()) {
            return new PoiDetailViewModel(visiblePois, removePois);
        }

        // add new pois to new boundary
        int count = 0;
        Iterator<Poi> iterator = pois.iterator();
        while (iterator.hasNext() && count < MAX_VISIBLE_MARKERS) {
            Poi next = iterator.next();
            if (mapBoundaryWrapper.isVisibleInBoundary(next) && !mapBoundaryWrapper.getMarkers().contains(next)) {
                visiblePois.add(next);
            }
            count++;
        }

        return new PoiDetailViewModel(visiblePois, removePois);
    }
}
