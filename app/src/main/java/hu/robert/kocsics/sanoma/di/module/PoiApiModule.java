package hu.robert.kocsics.sanoma.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.robert.kocsics.sanoma.data.service.PoiService;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

@Module(includes = {NetworkModule.class})
public class PoiApiModule {

    @Provides
    @Singleton
    PoiService providePoiApi(Retrofit retrofit) {
        return retrofit.create(PoiService.class);
    }

    @Provides
    @Singleton
    Scheduler provideScheduler() {
        return Schedulers.io();
    }
}
