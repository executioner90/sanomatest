package hu.robert.kocsics.sanoma.di.component;

import dagger.Subcomponent;
import hu.robert.kocsics.sanoma.presentation.detail.PoiDetailActivity;
import hu.robert.kocsics.sanoma.presentation.main.MainActivity;
import hu.robert.kocsics.sanoma.di.base.PerActivity;
import hu.robert.kocsics.sanoma.di.base.module.ActivityModule;

@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity mainActivity);

    void inject(PoiDetailActivity poiDetailActivity);

}
