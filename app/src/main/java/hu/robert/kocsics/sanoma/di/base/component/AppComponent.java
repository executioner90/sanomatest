package hu.robert.kocsics.sanoma.di.base.component;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import hu.robert.kocsics.sanoma.domain.PoiRepository;
import hu.robert.kocsics.sanoma.di.base.ApplicationContext;
import hu.robert.kocsics.sanoma.di.base.module.AppModule;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    @ApplicationContext
    Context context();

    Application application();

    PoiRepository apiManager();
}
