package hu.robert.kocsics.sanoma.presentation.detail.model;

import java.util.List;

import hu.robert.kocsics.sanoma.data.model.Poi;

public class PoiDetailViewModel {
    private List<Poi> poisToAdd;
    private List<Poi> poisToRemove;

    public PoiDetailViewModel(List<Poi> poisToAdd, List<Poi> poisToRemove) {
        this.poisToAdd = poisToAdd;
        this.poisToRemove = poisToRemove;
    }

    public List<Poi> getPoisToAdd() {
        return poisToAdd;
    }

    public List<Poi> getPoisToRemove() {
        return poisToRemove;
    }
}
