package hu.robert.kocsics.sanoma.presentation.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import hu.robert.kocsics.sanoma.R;
import hu.robert.kocsics.sanoma.data.model.Poi;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public class PoiAdapter extends RecyclerView.Adapter<PoiAdapter.PoiViewHolder> {

    private List<Poi> poiList;
    private Subject<Poi> poiClickSubject;

    @Inject
    PoiAdapter() {
        poiClickSubject = PublishSubject.create();
        poiList = Collections.emptyList();
    }

    public void setPoiList(List<Poi> poiList) {
        this.poiList = poiList;
        notifyDataSetChanged();
    }

    @Override
    public PoiViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_poi, parent, false);
        return new PoiViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PoiViewHolder holder, int position) {
        Poi poi = this.poiList.get(position);
        holder.onBind(poi);
    }

    @Override
    public int getItemCount() {
        return poiList.size();
    }

    Observable<Poi> getPoiClick() {
        return poiClickSubject;
    }

    class PoiViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_name)
        TextView nameText;

        private Poi poi;

        PoiViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> poiClickSubject.onNext(poi));
        }

        void onBind(Poi poi) {
            this.poi = poi;
            nameText.setText(poi.name);
        }
    }
}
