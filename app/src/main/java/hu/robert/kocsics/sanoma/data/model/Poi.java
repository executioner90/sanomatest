package hu.robert.kocsics.sanoma.data.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Poi implements Serializable {
    public String name;
    public double lat;
    @SerializedName("long")
    public double lon;
}
