package hu.robert.kocsics.sanoma.presentation.base;

public interface Presenter<V extends MvpView> {

    void attachView(V mvpView);

    void detachView();
}
