package hu.robert.kocsics.sanoma.di.base.module;

import android.app.Application;
import android.content.Context;

import dagger.Module;
import dagger.Provides;
import hu.robert.kocsics.sanoma.di.base.ApplicationContext;
import hu.robert.kocsics.sanoma.di.module.PoiApiModule;


@Module(includes = {PoiApiModule.class})
public class AppModule {
    private final Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    Application provideApplication() {
        return application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return application;
    }

}
