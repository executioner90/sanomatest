package hu.robert.kocsics.sanoma.presentation.detail;

import hu.robert.kocsics.sanoma.data.model.Poi;
import hu.robert.kocsics.sanoma.presentation.base.MvpView;
import hu.robert.kocsics.sanoma.presentation.detail.model.MapBoundaryWrapper;
import hu.robert.kocsics.sanoma.presentation.detail.model.PoiDetailViewModel;
import io.reactivex.Observable;

public interface PoiDetailView extends MvpView {

    void setToolbarTitle(String title);

    void displayMarkers(PoiDetailViewModel poiDetailViewModel);

    Observable<Boolean> mapLoaded();

    Observable<Poi> getPoiClick();

    Observable<MapBoundaryWrapper> onBoundaryChange();
}
