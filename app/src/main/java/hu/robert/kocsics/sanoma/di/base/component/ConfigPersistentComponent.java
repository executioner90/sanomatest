package hu.robert.kocsics.sanoma.di.base.component;

import dagger.Component;
import hu.robert.kocsics.sanoma.presentation.base.BaseActivity;
import hu.robert.kocsics.sanoma.presentation.base.BaseFragment;
import hu.robert.kocsics.sanoma.di.base.ConfigPersistent;
import hu.robert.kocsics.sanoma.di.base.module.ActivityModule;
import hu.robert.kocsics.sanoma.di.base.module.FragmentModule;
import hu.robert.kocsics.sanoma.di.component.ActivityComponent;

/**
 * A dagger component that will live during the lifecycle of an Activity or Fragment but it won't be
 * destroy during configuration changes. Check {@link BaseActivity} and {@link BaseFragment} to see
 * how this components survives configuration changes. Use the {@link ConfigPersistent} scope to
 * annotate dependencies that need to survive configuration changes (for example Presenters).
 */
@ConfigPersistent
@Component(dependencies = AppComponent.class)
public interface ConfigPersistentComponent {

    ActivityComponent activityComponent(ActivityModule activityModule);

    FragmentComponent fragmentComponent(FragmentModule fragmentModule);
}
