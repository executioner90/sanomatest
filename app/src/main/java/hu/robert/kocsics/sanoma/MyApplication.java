package hu.robert.kocsics.sanoma;

import android.app.Application;
import android.content.Context;

import com.squareup.leakcanary.LeakCanary;

import hu.robert.kocsics.sanoma.di.base.component.AppComponent;
import hu.robert.kocsics.sanoma.di.base.component.DaggerAppComponent;
import hu.robert.kocsics.sanoma.di.base.module.AppModule;
import hu.robert.kocsics.sanoma.di.module.NetworkModule;

public class MyApplication extends Application {

    private AppComponent appComponent;

    public static MyApplication get(Context context) {
        return (MyApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            LeakCanary.install(this);
        }
    }

    public AppComponent getComponent() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder()
                    .networkModule(new NetworkModule(BuildConfig.POI_API_URL))
                    .appModule(new AppModule(this))
                    .build();
        }
        return appComponent;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(AppComponent appComponent) {
        this.appComponent = appComponent;
    }
}
