package hu.robert.kocsics.sanoma.data.service;

import hu.robert.kocsics.sanoma.data.model.PoiListResponse;
import io.reactivex.Single;
import retrofit2.http.GET;

public interface PoiService {

    @GET("pois.json")
    Single<PoiListResponse> getPoiList();
}
